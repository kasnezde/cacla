#!/usr/bin/env python
from __future__ import print_function

import sys
import os
import tensorflow as tf
import numpy as np
import gym
import signal

from gym import wrappers
from gym_recording.wrappers import TraceRecordingWrapper

from cacla import Cacla

if __name__ == '__main__':
  RANDOM_SEED = 42

  env_name = 'MountainCarContinuous-v0'
  env = gym.make(env_name)

  env.seed(RANDOM_SEED)
  np.random.seed(RANDOM_SEED)
  tf.set_random_seed(RANDOM_SEED)

  cacla = Cacla(env,
                max_episodes=150,
                max_steps=1500,
                max_reward=100.0,
                gamma=0.9,
                var=1.0,
                sigma=1.0,
                sigma_annealing=True,
                alpha=0.001,
                beta=0.001,
                actor_learning_rate=0.00001,
                critic_learning_rate=0.00004,
                zeta=0.001
                )

  signal.signal(signal.SIGINT, cacla.signal_handler)
  cacla.run()
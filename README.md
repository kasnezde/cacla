# Continuous Actor-Critic Learning Automaton (CACLA)

## Introduction
The project implements the **Continuous Actor-Critic Learning Automaton (CACLA)** [^1] for continuous environments from [OpenAI Gym](https://gym.openai.com/ "OpenAI Gym") .

## Structure
The project consists of several modules:

* `__main__.py` - launcher of the CACLA algorithm, set the parameters here (see below)
* `cacla.py` - body of the algorithm, contains the main loop
* `actor.py` - implements the Actor
* `critic.py` - implements the Critic
* `plotting.py` - utility for creating summary graphs

## Usage instructions
For launching the algorithm with the default environment and parameters, use the command `./__main__.py` from the current directory or `python cacla` from the parent directory.

## Environment
The environment can be set in the variable `env_name` in `./__main__.py`. Keep in mind that the algorithm is designed for the environments with continuous action and observation spaces (e.g. *MountainCarContinuous-v0*, *LunarLanderContinuous-v0*, *BipedalWalker-v2*).

## Parameters
There are many hyper-parameters which have to be tuned for the specific environments. All the parameters are located in the constructor of the `Cacla` object in `__main__.py`.

* *max_episodes* - limit for the number of episodes
* *max_steps* - limit for the number of steps
* *max_reward* - scale of the reward
* *gamma* - discount factor
* *var* - var for CACLA+Var
* *zeta* - beta coefficient for CACLA+Var
* *sigma* - standard deviation for gaussian exploration
* *sigma\_annealing* - whether to gradually reduce the exploration factor
* *alpha* - normalizing factor for the critic
* *beta* - normalizing factor for the actor
* *actor\_learning\_rate* - actor SGD learning rate
* *critic\_learning\_rate* - critic SGD learning rate


[^1]:  VAN HASSELT, Hado; WIERING, Marco A. Reinforcement learning in continuous action spaces. In: Approximate Dynamic Programming and Reinforcement Learning, 2007. ADPRL 2007. IEEE International Symposium on. IEEE, 2007. p. 272-279.

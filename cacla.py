#!/usr/bin/env python
from __future__ import print_function

import sys
import os
import tensorflow as tf
import numpy as np
import gym

import plotting

from actor import Actor
from critic import Critic

np.set_printoptions(linewidth=160)

class Cacla(object):
  def __init__(self, env, max_episodes=10000, max_steps=1000, max_reward=100, gamma=0.9, var=0.1, sigma=0.1, sigma_annealing=False,
              alpha=1e-5, beta=1e-5, actor_learning_rate=0.01, critic_learning_rate=0.01, zeta=0.001):
    self.env = env
    self.max_episodes = max_episodes
    self.max_steps = max_steps
    self.gamma = gamma
    self.var = var
    self.sigma_orig = self.sigma = sigma
    self.sigma_annealing = sigma_annealing
    self.alpha = alpha
    self.beta = beta
    self.actor_learning_rate = actor_learning_rate
    self.critic_learning_rate = critic_learning_rate
    self.zeta = zeta
    self.dir = "./log"
    self.max_reward = max_reward
    self.render = False

  def scale_reward(self, reward):
    return reward / self.max_reward

  def run(self):
    print("Press Ctrl+C to render, Ctrl+\\ to stop")
    self.sess = tf.Session()

    self.actor = Actor(self.env, self.sess, beta=self.beta, learning_rate=self.actor_learning_rate)
    self.critic = Critic(self.env, self.sess, alpha=self.alpha, learning_rate=self.critic_learning_rate)

    stats = plotting.EpisodeStats(
        episode_lengths=np.zeros(self.max_episodes),
        episode_rewards=np.zeros(self.max_episodes)) 

    for i_episode in range(self.max_episodes):
      self.state = self.env.reset()
      self.total_reward = 0

      for t in range(self.max_steps):
        if self.render:
          self.env.render()

        self.action = self.actor.predict(self.state, self.sigma)[0]

        next_state, reward, done, _ = self.env.step(self.action)
        self.total_reward += reward

        stats.episode_rewards[i_episode] += reward
        stats.episode_lengths[i_episode] = t

        target = self.scale_reward(reward) + self.gamma * self.critic.predict(next_state)
        delta_t = target - self.critic.predict(self.state)

        num_it = int(np.ceil(np.nan_to_num(delta_t / np.sqrt(self.var)))[0,0])
        self.var = self.updateVar(delta_t)

        self.critic.update(self.state, delta_t)

        if delta_t > 0:
          for i in range(num_it):
            self.actor.update(self.state, self.action, self.sigma)

        if done:
            break

        self.state = next_state

      print("Episode {}".format(i_episode))
      print("Finished after {} timesteps".format(t+1))
      print("Reward for this episode: {}".format(self.total_reward))

      if self.sigma_annealing:
        self.sigma = (self.sigma_orig) * ((float(self.max_episodes) - i_episode)/self.max_episodes)
        print("Sigma %f" % self.sigma)

    self.print_params()

    print(stats)

    plt, fig2 = plotting.plot_episode_stats(stats, smoothing_window=10, noshow=True)
    plt.show(fig2)

  def print_params(self):
    print("gamma:", self.gamma)
    print("var:", self.var)
    print("sigma:", self.sigma)
    print("alpha:", self.alpha)
    print("beta:", self.beta)
    print("actor_learning_rate:", self.actor_learning_rate)
    print("critic_learning_rate:", self.critic_learning_rate)
    print("zeta:", self.zeta)

  def signal_handler(self, signal, frame):
      print('Toggling render...')
      self.render = not self.render

  def updateVar(self, delta_t):
    return (1 - self.zeta) * self.var + self.zeta * delta_t ** 2
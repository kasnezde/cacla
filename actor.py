#!/usr/bin/env python
from __future__ import print_function

import sys
import tensorflow as tf
import numpy as np
import os
import gym

class Actor:
    def __init__(self, env, sess, beta=1e-5, learning_rate=0.01, scope="actor"):
        self.env = env
        self.session = sess
        self.beta = beta
        self.learning_rate = learning_rate

        self.state_dim = env.observation_space.shape[0]
        self.action_dim = env.action_space.shape[0]
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

        self.train_iteration = 0
        self.max_gradient = sys.maxint

        with tf.name_scope("inputs"):
            self.state = tf.placeholder(tf.float32, (None, self.state_dim), name="state")
            self.action = tf.placeholder(tf.float32, (None, self.action_dim), name="action")
            self.sigma = tf.placeholder(tf.float32, name="sigma")
        
        self.init_train_op()

        sess.run(tf.global_variables_initializer())

    def network(self, state):
        with tf.variable_scope("actor_network"):
            W1 = tf.get_variable("W1", [self.state_dim, 20],
                               initializer=tf.random_normal_initializer(mean=0.0,stddev=0.1))
            b1 = tf.get_variable("b1", [20],
                               initializer=tf.constant_initializer(0))
            h1 = tf.nn.tanh(tf.matmul(state, W1) + b1)
            W2 = tf.get_variable("W2", [20, self.action_dim],
                               initializer=tf.random_normal_initializer(mean=0.0,stddev=0.1))
            b2 = tf.get_variable("b2", [self.action_dim],
                               initializer=tf.constant_initializer(0))
            p = tf.matmul(h1, W2) + b2

        return p

    def gaussian_exploration(self, action, sigma):
        with tf.name_scope("actor_gaussian_exploration"):
            action_expl = []

            for i in range(self.action_dim):
                norm_dist = tf.contrib.distributions.Normal(action[0][i], sigma)
                sample = norm_dist.sample(1)

                action_expl.append(sample[0])

            action_expl = tf.reshape(action_expl, [1,self.action_dim])
            return action_expl

    def init_train_op(self):
        self.predicted_action = self.network(self.state)
        self.predicted_action = self.gaussian_exploration(self.predicted_action, self.sigma)
        self.predicted_action = tf.clip_by_value(self.predicted_action, self.env.action_space.low, self.env.action_space.high)

        network_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="actor_network")

        with tf.name_scope("actor_loss"):
            self.loss = tf.losses.mean_squared_error(self.action, self.predicted_action)

        with tf.name_scope("actor_train"):
            gradients = self.optimizer.compute_gradients(self.loss, network_variables)

            for i, (grad, var) in enumerate(gradients):
                if grad is not None:
                    gradients[i] = (tf.clip_by_norm(self.beta * self.loss * grad, self.max_gradient), var)

            self.train_op = self.optimizer.apply_gradients(gradients)

    def predict(self, state, sigma):
        state = np.reshape(state, (1,self.state_dim))

        feed_dict = {
            self.state: state,
            self.sigma: sigma
        }

        return self.session.run(self.predicted_action, feed_dict=feed_dict)

    def update(self, state, action, sigma):
        state = np.reshape(state, (1,self.state_dim))
        action = np.reshape(action, (1,self.action_dim))

        feed_dict =  {
            self.state: state,
            self.action: action,
            self.sigma: sigma
        }

        self.session.run([self.train_op], feed_dict=feed_dict)

        self.train_iteration += 1
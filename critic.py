#!/usr/bin/env python
from __future__ import print_function

import sys
import tensorflow as tf
import numpy as np
import os
import gym

class Critic:
    def __init__(self, env, sess, alpha=1e-5, learning_rate=0.01, scope="critic"):
        self.env = env
        self.session = sess
        self.alpha = alpha
        self.learning_rate = learning_rate
        self.state_dim = env.observation_space.shape[0]
        self.optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        self.state = tf.placeholder(tf.float32, (None, self.state_dim), name="state")
        self.target = tf.placeholder(tf.float32, (None, 1), name="target")
        self.train_iteration = 0
        self.max_gradient = sys.maxint

        
        self.init_train_op()
        sess.run(tf.global_variables_initializer())

    def network(self,state):
        with tf.variable_scope("critic_network"):
            W1 = tf.get_variable("W1", [self.state_dim, 20],
                               initializer=tf.contrib.layers.xavier_initializer())
            b1 = tf.get_variable("b1", [20],
                               initializer=tf.constant_initializer(0))
            h1 = tf.nn.tanh(tf.matmul(state, W1) + b1)
            W2 = tf.get_variable("W2", [20, 1],
                               initializer=tf.contrib.layers.xavier_initializer())
            b2 = tf.get_variable("b2", [1],
                               initializer=tf.constant_initializer(0))
            v = tf.matmul(h1, W2) + b2
            return v

    def init_train_op(self):
        self.value = self.network(self.state)
        network_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="critic_network")

        with tf.variable_scope("critic_loss"):
            loss = tf.losses.mean_squared_error(self.target, self.value)

        with tf.variable_scope("critic_train"):
            gradients = self.optimizer.compute_gradients(loss, network_variables)

            for i, (grad, var) in enumerate(gradients):
                if grad is not None:
                    gradients[i] = (self.alpha * loss * grad, var)

            self.train_op = self.optimizer.apply_gradients(gradients)

    def predict(self, state):
        state = np.reshape(state, (1,self.state_dim))
        feed_dict = {self.state: state}
        return self.session.run(self.value, feed_dict=feed_dict)

    def update(self, state, target):
        state = np.reshape(state, (1,self.state_dim))

        feed_dict =  {
            self.state: state,
            self.target: target,
        }
        self.session.run([self.train_op], feed_dict=feed_dict)
        self.train_iteration += 1